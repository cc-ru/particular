local tableAdv = require("lib.table")
local json = require("lib.json")

local particleloader = {
  saving = false,
  distribTypes = {
    -- https://gitlab.com/cc-ru/academy/-/blob/master/src/systems/particle.rs
    ["Rectangle"] = "uniform",
    -- XD...
    ["Normal"] = "normal",
    ["EllipseEdge"] = "borderellipse",
    ["Ellipse"] = "ellipse",
    ["RectangleEdge"] = "borderrectangle",
  },
  blendingTypes = {
    "Multiply",
    "Alpha",
    "Subtract",
    "Add",
    "Lighten",
    "Darken",
    "Screen",
    "Replace"
  }
}

function particleloader.load(filename)
  local file = io.open(filename or "init.json", "rb")
  if file then
    return json.decode(file:read("*a"))
  else
    -- Linux fix
    local contents, _ = love.filesystem.read("init.json")
    if contents then
      return json.decode(contents)
    else
      error("init.json file not found it looks like you deleted everything. Crazy?")
    end
  end
  error("Json file not found")
end

function particleloader.save(particle, filename)
  particleloader.saving = true
  local file = io.open(filename, "wb")
  local partCopy = tableAdv.copy(particle)

  -- REMOVE EDITOR RELATED SETTINGS (!!!!!!!!!!!!!!!!!)
  table.remove(partCopy.initial_direction, 3)

  file:write(json.encode(partCopy))
  file:close()
  particleloader.saving = false
end
return particleloader