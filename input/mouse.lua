local mouse = {}
mouse.delay = 0

function mouse.process(dt)
    mouse.delay = mouse.delay + 1 * dt
end

function mouse.isDown(button, delay)
    delay = delay or 0.2
    if mouse.delay > delay and love.mouse.isDown(button) then
        mouse.delay = 0
        return true
    else
        return false
    end
end

function mouse.isEnter(x1,y1,w1,h1)
    local x, y = love.mouse.getPosition()
    return x1 < x+1 and
    x < x1+w1 and
    y1 < y+1 and
    y < y1+h1
end

return mouse
