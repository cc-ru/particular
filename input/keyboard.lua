local keyboard = {}

keyboard.delay = 0

function keyboard.process(dt)
    keyboard.delay = keyboard.delay + 1 * dt
    --print(keyboard.delay)
end

function keyboard.isDown(button, delay)
    local delay = delay or 0.2
    if keyboard.delay > delay and love.keyboard.isDown(button) then
        keyboard.delay = 0
        --print(button)
        return true
    else
        --print("false:", button)
        return false
    end
end

return keyboard
