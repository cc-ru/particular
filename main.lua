local slab = require("lib.slab")
local keyboard = require("input.keyboard")
local inspect = require("lib.inspect")
local iter = require("lib.updateIter")
local tableAdv = require("lib.table")
local settings = require("settings")
local ser = require("lib.binser")
local json = require("lib.json")
local partLoader = require("particleloader")

-- save/load
local savepath
local loadpath
-- PARTICLE
local system
local particle
local shownpicker
local img
-- Emiter settings
local running = true
local speed = 1
local scale = 32

local canvas

local windows = {
  saveDialog = false,
  loadDialog = false
}

local rawdata = {
  enabled = false,
  y = 15
}

-- Res
local font

local function removeButton(tbl)
  slab.SameLine()
  if slab.Button("-", {W = 16, H = 16}) then
    if #tbl > 1 then
      table.remove(tbl, #tbl)
    end
  end
end

local function saveDialog()
  if love.system.getOS() == "Linux" and not settings.forceUseSlab then
    savepath = require("lib.gtkfiledialog").save("Save particle system file")
    if savepath then
      partLoader.save(savepath)
    end
  else
    windows.saveDialog = true
  end
end

local function loadDialog()
  if love.system.getOS() == "Linux" and not settings.forceUseSlab then
    loadpath = require("lib.gtkfiledialog").open("Load particle file")
    if loadpath then
      partLoader.load(loadpath)
    end
  else
    windows.loadDialog = true
  end
end

local function inputBoxSetting(value, title, props)
  local props = props or {Text = tostring(value), NumbersOnly = true, Step = 0.01}
  slab.Text(title)
  slab.SameLine()
  if slab.Input(title, props) then
    if tonumber(slab.GetInputText()) then
      return tonumber(slab.GetInputText())
    end
  end
  slab.SameLine()
  slab.SameLine()
  if slab.Button("0", {W = 16, H = 16}) then
    return 0
  end
  slab.SameLine()
  if slab.Button("H!", {W = 16, H = 16, Tooltip = "Returns max value\nVery DANGEROUS button USE CAREFULLY"}) then
    -- Slab bug: math.huge not working, so return max 64 bit signed integer
    return 9223372036854775807
  end
  return value
end

local function inputBoxSettingString(value, title, props)
  local props = props or {Text = tostring(value)}
  slab.Text(title)
  slab.SameLine()
  if slab.Input(title, props) then
    if string.len(slab.GetInputText()) > 0 then
      return slab.GetInputText()
    end
  end
  slab.Separator()
  return value
end

local function inputBoxDeg(value, valueToConv, title)
  slab.Text(title)
  slab.SameLine()
  if slab.InputNumberSlider("Slider" .. title, math.deg(value or 10), 0, 360) then
    local rads = math.rad(tonumber(slab.GetInputText()))
    particle[valueToConv][1] = math.cos(rads)
    particle[valueToConv][2] = math.sin(rads)
    return rads
  end
  return value
end

local function autoSave()
  if not savepath then
    saveDialog()
  else
    partLoader.save(particle, savepath)
  end
end

local function initImage()
  img = love.graphics.newImage(particle.sprite.path)
  canvas = love.graphics.newCanvas(img:getWidth(), img:getHeight())
  system = love.graphics.newParticleSystem(canvas)
end

function love.load()
  print(love.filesystem.getSourceBaseDirectory())
  particle = partLoader.load()
  local file = io.open("settings.set", "rb")
  if file then
    local deser = file:read("*a")
    local loadedSettings = ser.deserializeN(deser)
    for k, v in pairs(loadedSettings) do
      settings[k] = v
    end
    file:close()
  end
  math.randomseed(os.time())
  slab.Initialize()
  initImage()
  font = love.graphics.newFont("fonts/OpenSans-Regular.ttf", 12)
  system:setBufferSize(15000)
  print(inspect(particle))
end

function love.draw()
  love.graphics.setBackgroundColor(settings.bgColor)
  love.graphics.setCanvas(canvas)
  love.graphics.clear()
  love.graphics.draw(img)
  love.graphics.setCanvas()

  love.graphics.setBlendMode(string.lower(particle.sprite.blend_mode), "premultiplied")
  love.graphics.draw(system, love.graphics.getWidth() * 0.5, love.graphics.getHeight() * 0.5, scale, scale)
  love.graphics.setBlendMode("alpha")
  if rawdata.enabled then
    love.graphics.print(inspect(particle), 10, rawdata.y)
  end
  slab.Draw()
end

function love.update(dt)
  keyboard.process(dt)
  slab.Update(dt)
  slab.PushFont(font)

  if slab.BeginMainMenuBar() then
    if slab.BeginMenu("File") then
      if slab.MenuItem("New") then
        particle = partLoader.load()
      end
      if slab.MenuItem("Open") then
        loadDialog()
      end
      slab.Separator()
      if slab.MenuItem("Save") then
        autoSave()
      end
      if slab.MenuItem("Save As") then
        windows.saveDialog = true
      end

      slab.Separator()

      if slab.MenuItem("Quit") then
        love.event.quit()
      end

      slab.EndMenu()
    end

    if slab.BeginMenu("Editor") then
      if slab.MenuItem("Force using slab filedialog: " .. tostring(settings.forceUseSlab)) then
        settings.forceUseSlab = not settings.forceUseSlab
      end

      if slab.MenuItem("Sleep (Debug only)") then
        partLoader.saving = not partLoader.saving
      end

      slab.EndMenu()
    end

    slab.EndMainMenuBar()
  end

  if windows.saveDialog then
    local result = slab.FileDialog({Type = "savefile", Title = "Save particle file"})

    if result.Button == "OK" then
      windows.saveDialog = false
      savepath = result.Files[1]
      partLoader.save(particle, savepath)
    end

    if result.Button == "Cancel" then
      windows.saveDialog = false
    end
  end

  if windows.loadDialog then
    local result =
      slab.FileDialog(
      {Type = "loadfile", Title = "Load particle file", Filters = {{"*.json", "Particle definition file"}}}
    )
    if result.Button == "OK" then
      loadpath = result.Files[1]
      windows.loadDialog = false
      particle = partLoader.load(loadpath)
    end

    if result.Button == "Cancel" then
      windows.loadDialog = false
    end
  end

  slab.BeginWindow("Emiter", {Title = "Emiter editor", AutoSizeWindow = false})
  slab.Text("Emiter settings:")
  if slab.Button("Running " .. tostring(running)) then
    running = not running
  end
  slab.SameLine()
  if slab.Button("Step", {Disabled = running, W = 32}) then
    system:update(dt)
  end
  slab.SameLine()
  if slab.Button("Stop", {W = 32, Tooltip = "Stops and fully resets particle system"}) then
    system:stop()
  end
  slab.SameLine()
  if slab.Button("Start", {W = 32}) then
    system:start()
  end
  speed =
    inputBoxSetting(
    speed,
    "Speed:",
    {Text = tostring(speed), NumbersOnly = true, Step = 0.01, Tooltip = "Editor-only particle simulation speed"}
  )
  slab.Text("Background color: ")
  slab.SameLine()
  slab.Rectangle({W = 100, H = 16, Color = settings.bgColor})
  if slab.IsControlClicked() then
    shownpicker = "background"
  end
  slab.Separator()
  slab.Indent(5)
  particle.emission_rate = inputBoxSetting(particle.emission_rate, "Emission rate:")
  particle.emitter_lifetime = inputBoxSetting(particle.emitter_lifetime, "Emiter lifetime:")
  slab.Text("Emission area:")
  slab.Text("Distance: ")
  slab.Separator()
  particle.emission_area.distance[1] = inputBoxSetting(particle.emission_area.distance[1], "X: ")
  particle.emission_area.distance[2] = inputBoxSetting(particle.emission_area.distance[2], "Y: ")
  slab.Separator()
  if
    slab.CheckBox(
      particle.rel_wrt_center,
      "Direction relative to center",
      {
        Tooltip = "Newly spawned particles will be oriented relative to the center of the emission area"
      }
    )
   then
    particle.rel_wrt_center = not particle.rel_wrt_center
  end
  slab.Text("Distribution: ")
  slab.SameLine()
  if slab.BeginComboBox("ListBoxDist", {Selected = particle.emission_area.distribution}) then
    for k, _ in pairs(partLoader.distribTypes) do
      if slab.TextSelectable(k) then
        particle.emission_area.distribution = k
      end
    end

    slab.EndComboBox()
  end
  slab.Text("Color blending: ")
  slab.SameLine()
  if slab.BeginComboBox("ListBoxColorBlend", {Selected = particle.sprite.blend_mode}) then
    for i, v in ipairs(partLoader.blendingTypes) do
      if slab.TextSelectable(v) then
        particle.sprite.blend_mode = v
      end
    end

    slab.EndComboBox()
  end

  slab.Text("FPS: " .. love.timer.getFPS())
  --system:setBufferSize(inputBoxSettingString(system:getBufferSize(), "Buffer size:"))
  slab.EndWindow()

  slab.BeginWindow("Particle", {Title = "Particle editor", AutoSizeWindow = false})
  slab.Indent(0)
  slab.Text("Size graph:")
  slab.SameLine()
  if slab.Button("+", {W = 16, H = 16}) then
    if #particle.particle_size_graph < 8 then
      table.insert(particle.particle_size_graph, particle.particle_size_graph[#particle.particle_size_graph])
    end
  end
  slab.SameLine()
  removeButton(particle.particle_size_graph)
  slab.Indent(5)
  for k, v in ipairs(particle.particle_size_graph) do
    particle.particle_size_graph[k] = inputBoxSetting(v, k)
  end
  slab.Indent(0)
  slab.Text("Color graph:")
  slab.SameLine()
  if slab.Button("+", {W = 16, H = 16}) then
    if #particle.particle_color_graph < 8 then
      table.insert(particle.particle_color_graph, particle.particle_color_graph[#particle.particle_color_graph])
    end
  end
  slab.SameLine()
  removeButton(particle.particle_color_graph)
  slab.SameLine()
  if slab.Button("+R", {W = 16, H = 16}) then
    if #particle.particle_color_graph < 8 then
      table.insert(particle.particle_color_graph, {math.random(), math.random(), math.random(), math.random()})
    end
  end
  slab.Indent(5)
  for k, v in ipairs(particle.particle_color_graph) do
    if slab.Button(k, {W = 16, H = 16}) then
      shownpicker = k
    end
    slab.SameLine()
    slab.Rectangle({W = 100, H = 16, Color = v})
    if slab.IsControlClicked() then
      shownpicker = k
    end
  end
  slab.Indent(0)
  -- sorry for this, inputBoxSetting not works for some reason
  for k, v in pairs(particle) do
    if type(v) == "table" and v.min then
      slab.Text(tostring(string.gsub(k, "_", " ")))
      slab.Separator()
      slab.Text("Min: ")
      slab.SameLine()
      if
        slab.Input("settingParticleMin" .. k, {Text = tostring(particle[k].min), W = 80, NumbersOnly = true}) and
          tonumber(slab.GetInputText())
       then
        particle[k].min = tonumber(slab.GetInputText())
      end
      slab.SameLine()
      slab.Text("Max: ")
      slab.SameLine()
      if
        slab.Input("settingParticleMax" .. k, {Text = tostring(particle[k].max), W = 80, NumbersOnly = true}) and
          tonumber(slab.GetInputText())
       then
        if particle[k].max < particle[k].min then
          particle[k].min = particle[k].max
        else
          particle[k].max = tonumber(slab.GetInputText())
        end
      end
      slab.Separator()
    end
  end
  particle.initial_direction[3] = inputBoxDeg(particle.initial_direction[3], "initial_direction", "Initial direction:")
  slab.Separator()
  particle.initial_spread = inputBoxSetting(particle.initial_spread, "Initial spread:")
  slab.Separator()
  slab.Text("Sprite: ")
  slab.Separator()
  if love.filesystem.getInfo(particle.sprite.path, "file") then
    slab.Image(
      "ObjectImage" .. particle.sprite.path,
      {
        Path = particle.sprite.path
      }
    )
    if slab.Button("Apply") then
      initImage()
    end
  else
    slab.Text("ERROR: Path not found: " .. particle.sprite.path)
  end
  particle.sprite.path = inputBoxSettingString(particle.sprite.path, "Path:")

  particle.sprite.collision_rect[1] = inputBoxSetting(particle.sprite.collision_rect[1], "Collision X0")
  particle.sprite.collision_rect[2] = inputBoxSetting(particle.sprite.collision_rect[2], "Collision Y0")
  particle.sprite.collision_rect[3] = inputBoxSetting(particle.sprite.collision_rect[3], "Collision X1")
  particle.sprite.collision_rect[4] = inputBoxSetting(particle.sprite.collision_rect[4], "Collision Y1")

  slab.EndWindow()

  if shownpicker then
    if shownpicker ~= "background" then
      local color = slab.ColorPicker({Color = particle.particle_color_graph[shownpicker]})

      if color.Button == "OK" then
        particle.particle_color_graph[shownpicker] = color.Color
        shownpicker = nil
      end

      if color.Button == "Cancel" then
        shownpicker = nil
      end
    else
      local color = slab.ColorPicker({Color = settings.bgColor})

      if color.Button == "OK" then
        settings.bgColor = color.Color
        shownpicker = nil
      end

      if color.Button == "Cancel" then
        shownpicker = nil
      end
    end
  end

  -- show progress dialog if we have at least 1 task
  if #iter.iterators > 0 then
    slab.BeginWindow("progress", {Title = "Task progress"})
    slab.Text(iter.iterators[1].title .. " " .. iter.iterators[1].count .. "/" .. iter.iterators[1].stop)
    if slab.Button("Cancel current task (unstable)") then
      table.remove(iter.iterators, 1)
    end
    slab.SameLine()
    if slab.Button("Cancel all tasks") then
      tableAdv.clear(iter.iterators)
    end
    slab.EndWindow()
  end

  if keyboard.isDown("f1") then
    rawdata.enabled = not rawdata.enabled
  end

  if not partLoader.saving then
    system:setEmissionRate(particle.emission_rate)
    system:setEmitterLifetime(particle.emitter_lifetime)
    -- Multiple size graph to 32 pixels
    local graphGame = {}

    for _, v in ipairs(particle.particle_size_graph) do
      table.insert(graphGame, (v * particle.particle_size.max) / scale)
    end

    system:setSizes(unpack(graphGame))

    system:setColors(unpack(particle.particle_color_graph))
    system:setParticleLifetime(particle.particle_lifetime.min, particle.particle_lifetime.max)
    system:setSpin(particle.particle_spin.min, particle.particle_spin.max)
    system:setRadialAcceleration(particle.particle_radial_acceleration.min, particle.particle_radial_acceleration.max)
    system:setLinearDamping(-particle.particle_linear_acceleration.max, -particle.particle_linear_acceleration.min)
    --system:setLinearAcceleration(particle.particle_linear_acceleration.min, particle.particle_linear_acceleration.max)
    system:setTangentialAcceleration(
      particle.particle_tangential_acceleration.min,
      particle.particle_tangential_acceleration.max
    )
    system:setSpeed(particle.particle_initial_speed.min, particle.particle_initial_speed.max)
    system:setRotation(particle.particle_initial_rotation.min, particle.particle_initial_rotation.max)
    -- love2d pass direction in radians, but in game particle direction in x and y =)
    system:setDirection(math.atan2(particle.initial_direction[1], particle.initial_direction[2]))
    system:setEmissionArea(
      partLoader.distribTypes[particle.emission_area.distribution],
      particle.emission_area.distance[1],
      particle.emission_area.distance[2],
      particle.emission_area.rotation,
      particle.rel_wrt_center
    )
    system:setSpread(particle.initial_spread)
    local varik = (1 - particle.particle_size.min / particle.particle_size.max) % 1
    system:setSizeVariation(varik)
    keyboard.process(dt)
    iter.update()
  else
    slab.BeginWindow("saving", {Title = "Saving"})
    slab.Text("Saving please do not turn off your system!!!!")
    slab.EndWindow()
  end

  if running then
    system:update(dt * speed)
  end
end

function love.wheelmoved(x, y)
  if rawdata.enabled then
    rawdata.y = rawdata.y + y * 10
  end
end

function love.quit()
  partLoader.saving = true
  local file = io.open("settings.set", "w")
  file:write(ser.serialize(settings))
  file:close()
  partLoader.saving = false
  return true
end
