local updateIter = {
  iterators = {}
}

function updateIter.create(start, stop, step, fn, title)
  table.insert(
    updateIter.iterators,
    {
      title = title or "Unknown task",
      stop = stop,
      step = step,
      count = start,
      itFunction = fn
    }
  )
end

function updateIter.createFill(x, y, w, h, fn)
  updateIter.create(
    x,
    w,
    1,
    function(x)
      updateIter.create(
        y,
        h,
        1,
        function(y)
          fn(x, y)
        end,
        "Filling: x: " .. x
      )
    end,
    "Filling: y"
  )
end

function updateIter.update()
  for k, v in ipairs(updateIter.iterators) do
    v.itFunction(v.count)
    v.count = v.count + v.step
    if v.count >= v.stop then
      table.remove(updateIter.iterators, k)
    end
  end
end

return updateIter
