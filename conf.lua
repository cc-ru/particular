function love.conf(t)
    t.title = "Particular"
    t.modules.joystick = false
    t.modules.physics = false
    t.window.resizable = true
    t.console = true
    t.vsync = false
end
